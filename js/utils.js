var abi = require('ethjs-abi')
var ethereumjsABI = require('ethereumjs-abi')
var Web3 = require('web3')
var web3 = new Web3(Web3.givenProvider || 'ws://localhost:8546')
var jsAssess = require('./simulateAssessment.js')
var FathomToken = artifacts.require('FathomToken')

const Stage = Object.freeze({
  None: 0,
  Called: 1,
  Confirmed: 2,
  Committed: 3,
  Done: 4,
  Burned: 5,
  Dissent: 6
})
exports.Stage = Stage

exports.zeroAddress = '0x0000000000000000000000000000000000000000'

exports.hashScoreAndSalt = function (_score, _salt) {
  return '0x' + ethereumjsABI.soliditySHA3(
    ['int128', 'string'],
    [_score, _salt]
  ).toString('hex')
}

exports.getNotificationArgsFromReceipt = function (_receipt, _topic) {
  var events = []
  var notificationIndex
  for (let i = 0; i < FathomToken.abi.length; i++) {
    if (FathomToken.abi[i].name === 'Notification' &&
        FathomToken.abi[i].type === 'event') {
      notificationIndex = i
    }
  }
  for (let i = 0; i < _receipt.rawLogs.length; i++) {
    // check whether event-signature (topic 0) matches the Notification-Event:
    if (_receipt.rawLogs[i].topics[0] === '0x61be7a191fce9c12da97e5ba5978e5d31e3a8374012c731682182c5102faac9d') {
      let decodedEvent = abi.decodeLogItem(FathomToken.abi[notificationIndex], _receipt.rawLogs[i])
      if (decodedEvent.topic.toNumber() === _topic) {
        events.push(decodedEvent)
      }
    }
  }
  return events
}

exports.evmIncreaseTime = function (seconds) {
  return new Promise(function (resolve, reject) {
    return web3.currentProvider.send({
      jsonrpc: '2.0',
      method: 'evm_increaseTime',
      params: [seconds],
      id: new Date().getTime()
    }, function (error, result) {
      return error ? reject(error) : resolve(result.result)
    })
  })
}

exports.getCalledAssessors = function (receiptFromMakeAssessment) {
  let calledAssessors = []
  let callsToAssessors = this.getNotificationArgsFromReceipt(receiptFromMakeAssessment, 1)
  for (let a = 0; a < callsToAssessors.length; a++) {
    calledAssessors.push(web3.utils.toChecksumAddress(callsToAssessors[a].user))
  }
  return calledAssessors
}

exports.getBalances = async function (_accounts, _userRegistryInstance) {
  let balances = []
  for (let i = 0; i < _accounts.length; i++) {
    let tmp = await _userRegistryInstance.balanceOf.call(_accounts[i])
    balances.push(tmp.toNumber())
  }
  return balances
}

exports.getEthBalances = function (_accounts) {
  let balances = []
  for (let i = 0; i < _accounts.length; i++) {
    balances.push(web3.eth.getBalance(_accounts[i]))
  }
  return balances
}

exports.weiToDollar = function (wei, etherPrice) {
  return web3.utils.fromWei(wei, 'ether') * etherPrice.toString()
}

exports.getRandomInt = function (min, max) {
  min = Math.ceil(min)
  max = Math.floor(max)
  return Math.floor(Math.random() * (max - min)) + min // The maximum is exclusive and the minimum is inclusive
}
/*
  helper function to generate a bunch of tickets for the minting lottery py pulling
  the relevant data from the assessments and generating the hashes
  @param assessments: list of assessments for which to generate tickets (entries are JSON
    objects returned from .js/assessmentFunctions.createAndRunAssessment())
  @param nAssessors: number of assessors to generate tickets for
  @param nSalts: number of tickets to generate per assessor
  */
exports.generateTickets = async function (assessments, nAssessors, nSalts) {
  let tickets = []
  for (let assessment of assessments) {
    let assessmentSaltHex = await assessment.instance.salt.call()
    let maxAss = Math.min(nAssessors, assessment.calledAssessors.length)
    for (let assessor of assessment.calledAssessors.slice(0, maxAss)) {
      let maxTickets = Math.min(nSalts, assessment.cost)
      for (let i = 0; i < maxTickets; i++) {
        tickets.push(
          {
            inputs: {
              assessor: assessor,
              assessment: assessment.address,
              tokenSalt: i,
              salt: assessmentSaltHex
            },
            hash: jsAssess.hashTicket(
              assessor,
              assessment.address,
              i,
              assessmentSaltHex)
          }
        )
      }
    }
  }
  return tickets
}

exports.functionReverts = async (call, reason = 'revert') => {
  try {
    await call()
  } catch (e) {
    if (e.toString().indexOf(reason) > 0) {
      return true
    }
    // for cases where reason string is provided but contains typo, or is otherwise mis-matching or invalid
    if (reason !== 'revert') {
      console.log(`provided reason ('${reason}') does not match error ('${e.toString()}')`)
      return false
    }
    // any other non-revert error cases
    console.log(e.toString())
    return false
  }
  console.log('function call did not revert!!!')
  return false
}
