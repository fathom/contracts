exports.NUMBER_OF_INITIALMEMBERS = 25

module.exports = async (network, web3, toBeAddedToMew) => {
  let initialAccounts
  switch (network) {
    case 'rinkeby':
    case 'rinkeby-fork':
    case 'ropsten':
    case 'mainnet':
    case 'kovan':
    case 'kovan-fork':
      if (process.env.INITIAL_ACCOUNTS) {
        initialAccounts = JSON.parse(process.env.INITIAL_ACCOUNTS)
      } else initialAccounts = require('../initialMembers.json')
      return initialAccounts
    case 'development':
      let accounts = (await web3.eth.getAccounts())
      if (toBeAddedToMew) {
        return accounts.slice(0, 25)
      }
      return accounts
    case 'default':
      throw Error('Unknown network! Please update getAccounts file or check your network')
  }
}
