module.exports = {
  port: 6545,
  skipFiles: ['mocks', 'lib', 'NewAssessment.sol', 'NewAssessmentData.sol']
}
