## 2.0.0 (21.06.2019) Eastward the wind blew

### BREAKING: 
   - Concept.sol: 
      - Renamed getter functions from get...Length() to getNumberOf...()
      - Renamed propagationRates to parentFactors
   - Renamed all function parameters to start with an underscore

### PROTOCOL:
   - The contracts of cancelled Assessments are no longer destroyed. Instead their stage is set to 'Burned'.
   - The finalScore of invalid assessments is no longer set to 0, but remains on whatever score was the average of the biggest cluster. Their status is set to 'Burned'.
   - Non-distributed tokens are actually burned and decrease the total supply
   
### BUGFIX:
   - Stealing can no longer freeze assessments by reducing their size
   - Check all conditions when changing parents
   
### MINTER: 
   - Added Waiting period after end of epoch for users to submit tickets from late assessments
   - No more minting to zero-address if there is no winner

### REFACTOR:
   - NatSpec comments for complex functions
   - Implemented solhint-linter & styleguide suggestions everywhere

## 1.3.0 (23.11.2018) A long expected party

### MINTER: 
  - Users can register "coinbase"-addresses to which minter-earnings should be redirected.

### BREAKING
  - The number of parents a concept can have is now limited to 5.
  - Propagation Rates now need to add up to 1000.
  - Notification-event signature is now ```event Notification(address indexed user, address indexed assessment, Note indexed topic)'''
  - Using 'stage' everywhere when talking about progress (renamed assessorState to assessorStage).

### PROTOCOL: 
  - Owners can now change a concept's parents.
  - Weights are no longer propagated upwards when an assessment is completed.
  - ConsentRadius, AssessorpoolSize and MinimalAssessorPoolSize *after calling* can are now parameterizable via the Assessment Contract.
  - Timelocked upgrade mechanism on the ProxyFacrory: The AssessmentMasterCopy, the owner of the Proxyfactory and the time an upgrade has to be announced ina advance before it can be implemented can be changed
  - Concepts can now be clones of other concepts & members can migrate their weight to the clones.

### REFACTOR:
  - Revert with reason implemented for better understanding of code failures

### SECURITY: 
  - Switch to using the [openzeppelin-solidity](https://github.com/OpenZeppelin/openzeppelin-solidity) implementation of ERC20.

### BUGFIX: 
  - corrected parameter order for assessment-creation.

## 1.2.2 (22.08.2018)

 - EVENTS: emit concept creation event when the MEW concept is created.
 
