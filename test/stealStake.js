var ConceptRegistry = artifacts.require('ConceptRegistry')
var FathomToken = artifacts.require('FathomToken')
var Assessment = artifacts.require('Assessment')

var utils = require('../js/utils.js')
var chain = require('../js/assessmentFunctions.js')

var nInitialUsers = 25
let aha
let assessmentContract
let cost = 1500
let size = 6
let timeLimit = 10000
let waitTime = 10

let calledAssessors
let confirmedAssessors

let scores = []
let salts = []
let hashes = []
for (let i = 0; i < nInitialUsers; i++) {
  scores.push(10)
  salts.push(i.toString())
  hashes.push(utils.hashScoreAndSalt(scores[i], salts[i]))
}

contract('Steal Stake:', function (accounts) {
  let assessee = accounts[nInitialUsers + 1]
  let outsideUser = accounts[nInitialUsers + 2]

  describe('without reducing the size of the assessment below minumum:', async () => {
    it('An assessment is created and users are called to be assessors.', async () => {
      let conceptReg = await ConceptRegistry.deployed()
      let txResult = await conceptReg.makeConcept(([await conceptReg.mewAddress()]), [1000], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress)
      let assessedConceptAddress = txResult.logs[0].args['_concept']
      aha = await FathomToken.deployed()

      // initiate assessment, save assessors and their initial balance
      let assessmentData = await chain.makeAssessment(assessedConceptAddress, assessee, cost, size, waitTime, timeLimit)
      assessmentContract = await Assessment.at(assessmentData.address)
      calledAssessors = assessmentData.calledAssessors

      assert.isAbove(calledAssessors.length, size - 1, 'not enough assessors were called')
    })

    it('Called assessors stake to confirm.', async () => {
      confirmedAssessors = calledAssessors.slice(0, size)
      let initialBalanceAssessors = await utils.getBalances(confirmedAssessors, aha)
      await chain.confirmAssessors(confirmedAssessors, assessmentContract)
      let balancesAfter = await utils.getBalances(confirmedAssessors, aha)
      assert.equal(balancesAfter[0], initialBalanceAssessors[0] - cost, 'stake did not get taken')

      let stage = await assessmentContract.assessmentStage.call()
      assert.equal(stage.toNumber(), utils.Stage.Confirmed, 'assessment did not move to stage confirmed')
    })

    it('Attempts to steal before Commit/Reveal stage should fail', async () => {
      let assessorStageBefore = (await assessmentContract.assessorStage.call(confirmedAssessors[1])).toNumber()
      await assessmentContract.steal(scores[1], salts[1], confirmedAssessors[1], { from: confirmedAssessors[0] })
      let assessorStageAfter = (await assessmentContract.assessorStage.call(confirmedAssessors[1])).toNumber()
      assert.equal(assessorStageBefore, assessorStageAfter, 'assessor has been burned despite not committing anything')
    })

    it('Assessors commit their hashed scores and the assessment advances.', async () => {
      await utils.evmIncreaseTime(60)
      await chain.commitAssessors(confirmedAssessors, hashes, assessmentContract)
      let stage = await assessmentContract.assessmentStage.call()
      assert.equal(stage.toNumber(), utils.Stage.Committed, 'assessment did not move to stage reveal')
    })

    it('After commit phase has ended, scores can no longer be committed', async () => {
      assert(await utils.functionReverts(assessmentContract.commit.bind(null, hashes[0], { from: confirmedAssessors[0] }), 'Called during wrong assessment stage'))
    })

    it('Cannot steal if hash (of stolen score + salt) does not match assessor-committed-hash', async () => {
      let assessorBalanceBefore = await aha.balanceOf.call(confirmedAssessors[1])
      // call steal from: confirmedAssessors[0], on: confirmedAssessors[1], but with score and salt of confirmedAssessors[2]
      await assessmentContract.steal(scores[2], salts[2], confirmedAssessors[1], { from: confirmedAssessors[0] })
      let assessorStageAfter = await assessmentContract.assessorStage.call(confirmedAssessors[1])
      let assessorBalanceAfter = await aha.balanceOf.call(confirmedAssessors[1])
      assert.notEqual(assessorStageAfter.toNumber(), utils.Stage.Burned, 'the assessor got burned')
      assert.equal(assessorBalanceBefore.toNumber(), assessorBalanceAfter.toNumber(), 'the assessor got burned despite invalid steal arguments')
    })

    describe('If assessors reveal their own score', function () {
      it('an error is thrown if they try to do so before the end of the 12h challenge period', async () => {
        assert(await utils.functionReverts(assessmentContract.reveal.bind(null, scores[0], salts[0], {from: confirmedAssessors[0]}), 'Challenge period must be over'))
      })

      it('they are marked as done, and the assessment progresses.', async () => {
        await utils.evmIncreaseTime(60 * 60 * 13) // let 12h challenge period pass
        let doneBefore = await assessmentContract.done.call()
        await assessmentContract.reveal(scores[0], salts[0], {from: confirmedAssessors[0]})
        let doneAfter = await assessmentContract.done.call()
        assert.equal(doneAfter.toNumber(), doneBefore.toNumber() + 1, 'the assessment did not progress')
      })
    })

    describe("If someone else steals an assessor's score", function () {
      var balanceBeforeSteal
      it('the assessor is burned and the size of the assessment reduced.', async () => {
        balanceBeforeSteal = await aha.balanceOf.call(outsideUser)

        let sizeBeforeSteal = await assessmentContract.size.call()
        await assessmentContract.steal(scores[1], salts[1], confirmedAssessors[1], {from: outsideUser})
        let sizeAfterSteal = await assessmentContract.size.call()
        assert.equal(sizeAfterSteal.toNumber(), sizeBeforeSteal.toNumber() - 1, "the assessment's size did not get reduced.")

        let burnedAssessorStage = await assessmentContract.assessorStage.call(confirmedAssessors[1])
        assert.equal(burnedAssessorStage.toNumber(), utils.Stage.Burned, 'the assessor did not get burned')
      })

      it('and his stake is given to the account who revealed it.', async () => {
        let balance = await aha.balanceOf.call(outsideUser)
        assert.equal(balance.toNumber(), balanceBeforeSteal.toNumber() + cost / 2, 'stake was not given to the stealer')
      })
    })

    describe('If an assessors waits too long to reveal his score', async () => {
      it('their stake is burned', async () => {
        let balanceBeforeLateReveal = await aha.balanceOf.call(confirmedAssessors[2])
        await utils.evmIncreaseTime(60 * 60 * 130) // let latest revealTime pass
        await assessmentContract.reveal(scores[2], salts[2], {from: confirmedAssessors[2]})
        let assessorStage = (await assessmentContract.assessorStage.call(confirmedAssessors[2])).toNumber()
        let balance = await aha.balanceOf.call(confirmedAssessors[2])
        assert.equal(balanceBeforeLateReveal.toNumber(), balance.toNumber(), 'Late revealing assessor was refunded too')
        assert.equal(assessorStage, utils.Stage.Burned, 'assessor did not get marked as burned')
      })
    })
  })
})

contract('Steal Stake:', function (accounts) {
  let assessors
  let assessment
  let size = 5
  let assessee = {address: accounts[size + 1]}
  let initialBalances
  let finalBalances

  describe('and thereby reducing the assessment size below minumum:', async () => {
    it('An assessment of size 5 is created and run until the reveal phase ', async () => {
      aha = await FathomToken.deployed()
      let conceptReg = await ConceptRegistry.deployed()

      assessee['balance'] = (await aha.balanceOf.call(assessee.address)).toNumber()
      let txResult = await conceptReg.makeConcept(([await conceptReg.mewAddress()]), [1000], 60 * 60 * 24, '0x00', utils.zeroAddress, utils.zeroAddress)
      let assessedConceptAddress = txResult.logs[0].args['_concept']
      let assessmentData = await chain.makeAssessment(assessedConceptAddress, assessee.address, cost, size, 1000, 2000)
      assessment = await Assessment.at(assessmentData.address)
      assessors = assessmentData.calledAssessors
      initialBalances = await utils.getBalances(assessors, aha)

      await chain.confirmAssessors(assessors.slice(0, size), assessment)
      utils.evmIncreaseTime(13)
      await chain.commitAssessors(assessors.slice(0, size), hashes, assessment)
      let stage = await assessment.assessmentStage.call()
      assert.equal(stage.toNumber(), utils.Stage.Committed, 'did not reach Committed stage')
    })

    it('as soon as one assessors stake is stolen the assessment is cancelled', async () => {
      let tx = await assessmentContract.steal(scores[1], salts[1], assessors[1], {from: assessors[2]})
      // if an assessment is cancelled, there was a cancellation event with topic 3 aka FathomToken.Note.AssessmentCancelled,
      let assessorStage = (await assessmentContract.assessorStage.call(assessors[1])).toNumber()
      assert.equal(assessorStage, utils.Stage.Burned, 'assessor did not get marked as burned')
      let cancelEvent = utils.getNotificationArgsFromReceipt(tx.receipt, 3)[0] // FathomToken.Note.CancelledAssessment
      assert(cancelEvent, 'no CancelEvent fired!')
    })

    it('the assessee is refunded', async () => {
      let balanceAfterRefund = await aha.balanceOf.call(assessee.address)
      assert.equal(assessee.balance, balanceAfterRefund.toNumber(), 'assessee did not get refunded')
    })

    it('the other assessors are refunded', async () => {
      finalBalances = await utils.getBalances(assessors, aha)
      assert.equal(initialBalances[3], finalBalances[3])
    })

    it('the assessor whose stake got stolen is not', async () => {
      assert.equal(initialBalances[1], finalBalances[1] + cost)
    })

    it('and no tokens stay in the assessment contract', async () => {
      let leftOverTokens = (await aha.balanceOf(assessmentContract.address)).toNumber()
      assert.equal(leftOverTokens, 0, 'not all tokens got burned')
    })
  })
})
