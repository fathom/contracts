var FathomToken = artifacts.require('./FathomToken.sol')
let getAccounts = require('../js/getAccounts.js')

module.exports = function (deployer) {
  deployer.then(async () => {
    let accounts = await getAccounts(deployer.network, web3)
    let instance = await FathomToken.deployed()
    let amount = 100e9
    console.log('funding', accounts.length, 'users with', amount / 1e9, 'AHA each')
    for (let i = 1; i < accounts.length; i++) {
      await instance.transfer(accounts[i], amount)
      if (deployer.network !== 'development') console.log('funded', accounts[i])
    }
  })
}
