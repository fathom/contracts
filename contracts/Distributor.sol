pragma solidity ^0.5.0;

import "./Concept.sol";
import "./ConceptRegistry.sol";


/// @notice Contract that can add a given number of members to the mew-concept.
/// @dev Build for one-time use during network deployment
contract Distributor {
    uint public nInitialMembers;
    address public conceptRegistry;
    address[] public memberAddresses;
    uint[] public memberWeights;

    constructor(uint _nInitialMembers, address _conceptRegistry) public {
        nInitialMembers = _nInitialMembers;
        conceptRegistry = _conceptRegistry;
    }

    function addInitialMember(address _memberAddress, uint _memberWeight) public {
        require(memberAddresses.length < nInitialMembers, "Max number of initial members already added");
        Concept(ConceptRegistry(conceptRegistry).mewAddress()).addInitialMember(_memberAddress, _memberWeight);
        memberAddresses.push(_memberAddress);
        memberWeights.push(_memberWeight);
    }
}

