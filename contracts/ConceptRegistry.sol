pragma solidity ^0.5.0;

import "./Concept.sol";
import "./ProxyFactory.sol";


/**
   @notice Central contract of the fathom network, creates and stores the
   concept ontology
 */
contract ConceptRegistry {

    mapping (address => bool) public conceptExists;

    address public fathomToken;

    bool public initialized = false;

    // MEW is the first concept that exists, it has no parents
    address public mewAddress;

    // The address which is allowed to add members to MEW without assessment
    address public distributorAddress;

    ProxyFactory public proxyFactory;

    event ConceptCreation (address _concept, address indexed cloneOf);

    ///@notice Creates the MEW concept and links other relevant contracts
    function init(address _token, address _distributor, address _proxyFactory) public {
        if (initialized == false) {
            fathomToken = _token;
            distributorAddress = _distributor;
            proxyFactory = ProxyFactory(_proxyFactory);
            mewAddress = proxyFactory.createConcept(
                new address[](0),
                new uint[](0),
                uint(2**255),
                "",
                address(0x0),
                address(0x0)
            );
            emit ConceptCreation(mewAddress, address(0x0));
            conceptExists[mewAddress] = true;
            initialized = true;
        }
    }

    /*
      @notice: To create a new concept
      @param _parentList: An array of addresses containing the addresses of the concepts parents
      @param _parentFactors: List of degrees of similarity to each parent (must add up to 1000)
      @param _lifetime: How long assessments in this concept remain valid
      @param _data: Can be used to define what the concept is about
      @param _owner: Address which can change the concept lifetime, data and parents
      @param cloneOf: Address of the concept that this concept has been cloned of
      @return the address of the new concept
    */
    function makeConcept (
        address[] memory _parentList,
        uint[] memory _parentFactors,
        uint _lifetime,
        bytes memory _data,
        address _owner,
        address cloneOf
    )
        public
        returns (address)
    {
        require(_parentList.length > 0, "Must specify at least one parent concept");
        address newConceptAddress = proxyFactory.createConcept(
            _parentList,
            _parentFactors,
            _lifetime,
            _data,
            _owner,
            cloneOf
        );

        conceptExists[newConceptAddress] = true;
        emit ConceptCreation(newConceptAddress, cloneOf);
        return newConceptAddress;
    }
}
