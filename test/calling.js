var ConceptRegistry = artifacts.require('ConceptRegistry')
var Concept = artifacts.require('Concept')
let assess = require('../js/assessmentFunctions.js')
let utils = require('../js/utils.js')

let nInitialUsers = 25

contract('Calling from parent concepts: ', (accounts) => {
  let conceptRegistry
  let assessedConcept
  let mewAddress
  let mewConcept
  let assessment
  let userA = accounts[nInitialUsers + 1]
  let userB = accounts[nInitialUsers + 2]

  it('Initially, create a new concept and have user A pass an assessment in it', async () => {
    // create concept
    conceptRegistry = await ConceptRegistry.deployed()
    mewAddress = await conceptRegistry.mewAddress.call()
    let name1 = web3.utils.asciiToHex('banana')
    let txReciept = await conceptRegistry.makeConcept([mewAddress], [1000], 60 * 60 * 24, name1, accounts[0], utils.zeroAddress)
    let createdConceptAddress = txReciept.logs[0].args['_concept']
    assessedConcept = await Concept.at(createdConceptAddress)
    assert.isTrue(await conceptRegistry.conceptExists.call(createdConceptAddress), 'created concept doesn\'t exist')
    // run Assessment
    assessment = await assess.createAndRunAssessment(
      assessedConcept.address,
      userA, // assessee
      1000, // cost
      5, // size
      100, // waitTime
      100000, // endTime
      [90, 90, 90, 95, 100], // scores
      -1 // use default Salts
    )
    let stage = await assessment.instance.assessmentStage.call()
    assert.equal(stage.toNumber(), 4, 'assessment did not move to stage done')
  })

  it('By default, user A is not available and is therefore not called as assessor ', async () => {
    assert.isTrue(await assess.assessorIsNotCalled(userA, 10, assessedConcept.address, accounts[nInitialUsers + 3]))
  })

  it('Assessors for the new concept are called from the parent', async () => {
    assert.isTrue(await assess.assessorIsCalled(accounts[1], 10, assessedConcept.address, userA))
  })

  it('After setting themselves unavailable users are not called as assessor', async () => {
    mewConcept = await Concept.at(mewAddress)
    await mewConcept.toggleAvailability(false, {from: accounts[1]})
    assert.isTrue(await assess.assessorIsNotCalled(accounts[1], 10, mewAddress, userA))
  })

  it('Users, whose positions have been changed by other users marking themselves as unavailable are still called', async () => {
    let lastMemberIndex = (await mewConcept.getNumberOfAvailableMembers.call()).toNumber()
    assert.isTrue(await assess.assessorIsCalled(accounts[lastMemberIndex], 10, mewAddress, userA))
  })

  it('After toggling available, user A IS called as assessor', async () => {
    // Because we never draw more than half the number of assessors, another user has to get assessessed
    // and toggle available, before user A can be drawn as assessor
    assessment = await assess.createAndRunAssessment(
      assessedConcept.address,
      userB, // assessee
      1000, // cost
      5, // size
      100, // waitTime
      100000, // endTime
      [90, 90, 90, 95, 100], // scores
      -1 // use default Salts
    )
    let weightB = (await assessedConcept.getWeight.call(userB)).toNumber()
    assert.isAbove(weightB, 0, 'userB did not pass the assessment')
    await assessedConcept.toggleAvailability(true, {from: userA})
    await assessedConcept.toggleAvailability(true, {from: userB})

    assert.isTrue(await assess.assessorIsCalled(userA, 10, assessedConcept.address, accounts[0]))
  })
})
