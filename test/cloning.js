var ConceptRegistry = artifacts.require('ConceptRegistry')
// var FathomToken = artifacts.require('FathomToken')
var Concept = artifacts.require('Concept')
let assessmentFunctions = require('../js/assessmentFunctions.js')
let utils = require('../js/utils.js')

var conceptRegistry

contract('Cloning', function (accounts) {
  var mewAddress
  var mewConcept
  var cloneConcept
  let userA = accounts[0]

  it('user A is assessed in mew', async () => {
    conceptRegistry = await ConceptRegistry.deployed()
    mewAddress = await conceptRegistry.mewAddress.call()
    await assessmentFunctions.createAndRunAssessment(mewAddress, userA, 5, 5, 50, 1000, [100, 100, 100, 100, 100])
  })

  it('a clone of mew is created', async () => {
    await utils.evmIncreaseTime(600)
    let txReceipt = await conceptRegistry.makeConcept([mewAddress], [1000], 60 * 60 * 24, '0x00', utils.zeroAddress, mewAddress)

    cloneConcept = await Concept.at(txReceipt.logs[0].args['_concept'])
    assert.equal(await cloneConcept.cloneOf.call(), mewAddress, "Clone does not know it's original")
  })

  it('it reverts if the clone is not a network-concept', async () => {
    assert(utils.functionReverts(
      conceptRegistry.makeConcept.bind(null, [mewAddress], [1000], 60 * 60 * 24 * 7, '0x00', utils.zeroAddress, accounts[0]),
      'Desired original is not valid'
    ))
  })

  it('a member who was assessed in mew (user A) can migrate their weight to the clone', async () => {
    mewConcept = await Concept.at(mewAddress)
    let weightBefore = await cloneConcept.getWeight.call(userA)
    assert.equal(weightBefore.toNumber(), 0, 'member already has a weight')

    // be available as Assessor in the cloned concept
    await cloneConcept.integrateMember({from: userA})
    let weightAfter = await cloneConcept.getWeight.call(userA)
    assert.isAbove(weightAfter.toNumber(), 0, 'member did not get a weight')
  })

  it('is no longer member of mew', async () => {
    let weightAfter = await mewConcept.getWeight.call(userA)
    assert.equal(weightAfter.toNumber(), 0)
  })

  it('and is available in the new concept, if they toggle available', async () => {
    await cloneConcept.toggleAvailability(true, {from: userA})
    let availableMembers = await cloneConcept.getAvailableMembers.call()
    assert.equal(availableMembers[0], accounts[0], 'member was not made available') // or the other member was also made available
  })
})
