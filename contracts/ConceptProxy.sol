pragma solidity ^0.5.0;

// NOTE:
// The architecture of Concept- ConceptData- and ConceptProxy-contract
// has been modelled upon
// https://blog.gnosis.pm/solidity-delegateproxy-contracts-e09957d0f201 by Alan Lu

import "./Proxy.sol";
import "./ConceptData.sol";
import "./ConceptRegistry.sol";


/**
   @dev ProxyContract for concepts. For the concept logic see
   ./Concept.sol and for state declarations see ./ConceptData.sol
*/
contract ConceptProxy is Proxy, ConceptDataInternal {
    constructor(
        address _proxied,
        address _conceptRegistry,
        address[] memory _parents,
        uint[] memory _parentFactors,
        uint _lifetime,
        bytes memory _data,
        address _owner,
        address _cloneOf
    )
        public
        Proxy(_proxied)
    { //solhint-disable-line bracket-align
        require(_parents.length == _parentFactors.length,
            "Number of parent concepts must match parent factors"
        );
        require(_parents.length < 6, "Too many parents"); // NOTE: this value is a working assumption
        conceptRegistry = ConceptRegistry(_conceptRegistry);

        uint sumOfParentFactors = 0;
        for (uint j=0; j < _parents.length; j++) {
            require(conceptRegistry.conceptExists(_parents[j]), "Parent concept does not exist");
            require(_parentFactors[j] <= 1000, "Parent factor must be <1000");
            sumOfParentFactors += _parentFactors[j];
        }
        // special case for mew-concept
        if (_parents.length > 0) {
            require(sumOfParentFactors == 1000, "Parent factors do not add up to 1000");
        }
        if (_cloneOf != address(0x0)) {
            require(conceptRegistry.conceptExists(_cloneOf), "Desired original is not valid");
            cloneOf = _cloneOf;
        }

        creationDate = now;
        parentFactors = _parentFactors;
        parents = _parents;
        data = _data;
        lifetime = _lifetime;
        owner = _owner;
        fathomToken = FathomToken(conceptRegistry.fathomToken());
    }
}
