var ConceptRegistry = artifacts.require('ConceptRegistry')
var Concept = artifacts.require('Concept')
let utils = require('../js/utils.js')

var conReg

contract('Changing data on Concepts: ', (accounts) => {
  let createdConcept
  let mewAddress
  let newConceptAddress

  it('Initially, create a new concept', async () => {
    conReg = await ConceptRegistry.deployed()
    mewAddress = await conReg.mewAddress.call()
    let name1 = web3.utils.asciiToHex('banana')
    let txReciept = await conReg.makeConcept([mewAddress], [1000], 60 * 60 * 24, name1, accounts[0], utils.zeroAddress)
    let createdConceptAddress = txReciept.logs[0].args['_concept']
    createdConcept = await Concept.at(createdConceptAddress)
    assert.isTrue(await conReg.conceptExists.call(createdConceptAddress), "mew doesn't exist")
  })
  describe('Concepts..', async () => {
    it('should allow only the owner to change lifetime of the concept', async () => {
      let ltBefore = await createdConcept.lifetime.call()
      await createdConcept.changeLifetime(10000)
      let ltAfter = await createdConcept.lifetime.call()
      assert.notEqual(ltBefore, ltAfter)
      assert(await utils.functionReverts(createdConcept.changeLifetime.bind(null, 4, {from: accounts[2]}), 'Owner access only'))
    })

    it('should allow only the owner to change the data', async () => {
      let newData = web3.utils.asciiToHex('newData!')
      await createdConcept.changeData(newData)

      let dataAfter = await createdConcept.data()
      assert(dataAfter === newData)
      assert(await utils.functionReverts(createdConcept.changeData.bind(null, '0x00', {from: accounts[2]}), 'Owner access only'))
    })

    it('should allow only the owner to change the concepts parents', async () => {
      // create a new concept to be the parent of the old one
      let txReciept = await conReg.makeConcept([mewAddress], [1000], 60 * 60 * 24, web3.utils.asciiToHex('ahah'), accounts[0], utils.zeroAddress)
      newConceptAddress = txReciept.logs[0].args['_concept']
      // make new Concept the parent
      let parentBefore = await createdConcept.parents.call(0)
      await createdConcept.changeParents([newConceptAddress], [1000], {from: accounts[0]})
      let parentAfter = await createdConcept.parents.call(0)
      assert.notEqual(parentBefore, parentAfter)
      assert.equal(parentAfter, newConceptAddress, 'new parent did not get saved')
      assert(await utils.functionReverts(createdConcept.changeParents.bind(null, [mewAddress], [100], {from: accounts[2]}), 'Owner access only'))
    })

    it('should only allow parent-changes that are within the limits of concept-creation', async () => {
      assert(await utils.functionReverts(createdConcept.changeParents.bind(null, [createdConcept.address], [1000], {from: accounts[0]}), 'Concept cannot be parent to itself'), 'concept could be parent to itself')
      assert(await utils.functionReverts(createdConcept.changeParents.bind(null, [mewAddress], [1001], {from: accounts[0]}), 'Parent factor must be <=1000'), 'illegal ParentFactor allowed')
      assert(await utils.functionReverts(createdConcept.changeParents.bind(null, [accounts[3]], [1000], {from: accounts[0]}), 'Parent concept does not exist'), 'nonexistent parent concept accepted')
      assert(await utils.functionReverts(createdConcept.changeParents.bind(null, [mewAddress, newConceptAddress], [500, 600], {from: accounts[0]}), 'Parent factors do not add up to 1000'), 'invalid sum of parentFactors was accepted')
    })

    it('should allow ownership to be transferred', async () => {
      await createdConcept.transferOwnership(accounts[1])
      let newOwner = await createdConcept.owner.call()
      assert.equal(accounts[1], newOwner)
    })
  })
})
