import numpy as np

def ui2block (x):
    return ((x+128)*100/254)

def block2ui (x):
    return ( x-50 ) * 127 / 50

row = [ui2block(block2ui(a)) for a in np.arange(100)]
row2 = [block2ui(ui2block(a)) for a in np.arange(-127,127)]

print row
print row2
