var ConceptRegistry = artifacts.require('ConceptRegistry')
var Concept = artifacts.require('Concept')
var utils = require('../js/utils.js')

contract('Concept Methods', function (accounts) {
  let conceptReg
  let assessedConcept

  describe('getParentsLength', async () => {
    it('returns the number of parents for a concept', async () => {
      conceptReg = await ConceptRegistry.deployed()
      assessedConcept = await Concept.at(await conceptReg.mewAddress())
      assert.isTrue(await conceptReg.conceptExists.call(assessedConcept.address))
      let parentsLength = await assessedConcept.getNumberOfParents.call()
      assert.isTrue(typeof parentsLength.toNumber() === 'number')
      assert.equal(parentsLength.toNumber(), 0, 'mewAddress should have no parents')
    })
  })

  describe('addInitialMember', async () => {
    it('can only be called by distributorAddress in conceptRegistry', async () => {
      let caller = accounts[1]
      assert.notEqual((await conceptReg.distributorAddress.call()), caller)
      assert(await utils.functionReverts(assessedConcept.addInitialMember.bind(null, accounts[1], 1, {from: caller}), 'Distributor access only'))
    })
  })
})
